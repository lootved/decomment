package main

import (
	"bufio"
	"bytes"
	"flag"
	"fmt"
	"log"
	"os"
	"strings"
)

var input string
var output string
var sep string

func main() {
	flag.StringVar(&input, "p", "", "input file to process")
	flag.StringVar(&output, "o", "", "output file location (default: input_name.new)")
	flag.StringVar(&sep, "s", "//", "comment character used to discard lines")

	flag.Parse()
	if input == "" {
		fmt.Println("missing mandatory parameter input path, provide it with -p")
		os.Exit(1)
	}
	if output == "" {
		output = input + ".out"
	}

	ipf, err := os.Open(input)
	if err != nil {
		log.Fatalln(err)
	}
	defer ipf.Close()

	scanner := bufio.NewScanner(ipf)

	var raw []byte
	var line string
	var buff bytes.Buffer
	newLine := []byte{'\n'}
	for scanner.Scan() {
		raw = scanner.Bytes()
		line = string(raw)
		line = strings.TrimSpace(line)
		if !strings.HasPrefix(line, sep) {

			buff.Write(raw)
			buff.Write(newLine)
		}
	}

	n := buff.Len()
	res := buff.Bytes()[:n-1]
	f, err := os.Create(output)
	if err != nil {
		log.Fatal(err)
	}

	defer f.Close()
	f.Write(res)
}
